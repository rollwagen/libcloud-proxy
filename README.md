

Python pre-requisites / module dependencies
------------------------------------------
* apache-libcloud
* ipython


Python: Comprehension
---------------------
	images = amazon.list_images()
	# images is a list of <NodeImage: id=aki-00af8574, name=None, driver=Amazon EC2 (eu-west-1)  ...>
	# Comprehensions example:  all image IDs in a list
	col1 = [row.id for row in images]

Python: Lists
-------------

	node_list = [n for n in nodes if n.uuid == '492a874a9e1a22f0f8dc20b3639d50f7a07f92e0']
