import os
import logging
import time
from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver


ACCESS_ID = os.environ["AWS_ACCESS_KEY"]
SECRET_KEY = os.environ['AWS_SECRET_KEY']

#IMAGE_ID = 'ami-ce7b6fba' #ubuntu
IMAGE_ID = 'ami-8d1109f9'
SIZE_ID = 't1.micro'  #m1.xlarge' m1.large' 't1.micro'
KEYPAIR_NAME = 'rollkey'
SECURITY_GROUP_NAMES = 'ssh_from_home'


class Timer:

    def __init__(self, func=time.perf_counter):
                self.elapsed = 0.0
                self._func = func
                self._start = None

    def start(self):
        if self._start is not None:
            raise RuntimeError('Already started')
        self._start = self._func()

    def stop(self):
        if self._start is None:
            raise RuntimeError('Not started')
        end = self._func()
        self.elapsed += end - self._start
        self._start = None

    def reset(self): self.elapsed = 0.0

    @property
    def running(self):
        return self._start is not None

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, *args):
        self.stop()


def main():
    logging.basicConfig(
        # filename="libcloud.log",
        level=logging.INFO,
        format='%(message)s'
    )

    cls = get_driver(Provider.EC2_EU_WEST)
    amazon = cls(ACCESS_ID, SECRET_KEY)

    sizes = amazon.list_sizes()
    images = amazon.list_images()

    size = [s for s in sizes if s.id == SIZE_ID][0]
    image = [i for i in images if i.id == IMAGE_ID][0]
    logging.info("Using size with id = %s (%s)", size.id, size.name)
    logging.info("Using image with id = %s", image.id)

    t = Timer()
    t.start()
    node = amazon.create_node(name='test-node', image=image, size=size, ex_keyname=KEYPAIR_NAME, ex_securitygroup=SECURITY_GROUP_NAMES)
    amazon.wait_until_running([node])
    t.stop()
    logging.info("Created node: %s", node)
    logging.info("Node creation time %0.2f seconds", t.elapsed)



if __name__ == '__main__':
    main()

