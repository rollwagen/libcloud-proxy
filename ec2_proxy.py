import os
import logging
import time
import sys
# import urllib2

from libcloud.compute.deployment import MultiStepDeployment
from libcloud.compute.deployment import ScriptDeployment
from libcloud.compute.deployment import SSHKeyDeployment
from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

from IPython.terminal.embed import InteractiveShellEmbed


ACCESS_ID = os.environ["AWS_ACCESS_KEY"]
SECRET_KEY = os.environ['AWS_SECRET_KEY']
EC2_KEY = "id_rsa"
EC2_ZONE = Provider.EC2_US_EAST
IMAGE_ID = 'ami-d85e75b0'  # ubuntu
#IMAGE_ID = 'ami-a9184ac0'  # ubuntu
SIZE_ID = 't1.micro'
SECURITY_GROUP_NAMES = 'ssh_proxy_from_home'
KEYPAIR_NAME = 'id_rsa'

BASH_CONFIGURE_PROXY = """#!/bin/bash
sudo apt-get install -y squid3
sudo sed -i.bak 's/^http_access\ deny\ all/http_access\ allow\ all/' /etc/squid3/squid.conf
sudo service squid3 restart
"""

def _init_logging():
    logging.basicConfig(
        #filename="libcloud.log",
        level=logging.DEBUG,
        format='%(message)s'
	# to enable libcloud debug log, on command line:
    # LIBCLOUD_DEBUG=libcloud.log python ec2_proxy.py
    )


def main():
    _init_logging()

    cls = get_driver(EC2_ZONE)
    amazon = cls(ACCESS_ID, SECRET_KEY)

    sizes = amazon.list_sizes()
    size = [s for s in sizes if s.id == SIZE_ID][0]
    logging.info("Using size with id = %s (%s)", size.id, size.name)

    images = amazon.list_images()
    image = [i for i in images if i.id == IMAGE_ID][0]
    logging.info("Using image with id = %s", image.id)

    node = amazon.create_node(name='proxy', image=image, size=size, ex_keyname=KEYPAIR_NAME,
                              ex_securitygroup=SECURITY_GROUP_NAMES, ex_userdata=BASH_CONFIGURE_PROXY)
    logging.info("Creating node: %s", node)
    amazon.wait_until_running([node])
    logging.info("Finished creating node. Node ID=%s", node.id)

    public_ips = [n.public_ips for n in amazon.list_nodes() if n.uuid == node.uuid][0]
    if len(public_ips) > 0:
        logging.info("Public IP of node = %s", public_ips[0])
    else:
        logging.info("Could not retrieve node's public IP")

    if len(sys.argv) > 1 and sys.argv[1] == "-i":
        arg = sys.argv[1]
        logging.info("Command line argument ='%s' (interactive), starting interactive shell", arg)
        shell = InteractiveShellEmbed(banner1='Hello from Libcloud Shell !!')
        shell()


def verify_security_group():
    _my_ip = get_my_ip()
    logging.info("Workstation gateway IP = %s", _my_ip)
    # TODO: finish implementation i.e. check if security group defined as SECURITY_GROUP_NAMES contains correct IP


def get_my_public_ip():
    _my_ip = urllib2.urlopen('http://ip.42.pl/raw').read()
    logging.info("My IP = %s", _my_ip)
    return _my_ip


if __name__ == '__main__':
    main()
